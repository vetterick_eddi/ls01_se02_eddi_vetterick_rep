public class Benutzer {

    private int benutzername;
    private String vorname;
    private String nachname;
    private int geburtsjahr;
    private int alter;

    public Benutzer(int benutzername, String vorname, String nachname, int geburtsjahr, int alter) {
        this.benutzername = benutzername;
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsjahr = geburtsjahr;
        this.alter = alter;
    }

	public void setBenutzername(int benutzername) {
        this.benutzername = benutzername;
    }

    public int getBenutzername() {
        return benutzername;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setGeburtsjahr(int geburtsjahr) {
        this.geburtsjahr = geburtsjahr;
    }

    public int getGeburtsjahr() {
        return geburtsjahr;
    }
    
    public void setAlter(int alter) {
        this.alter = alter;
    }

    public int getAlter() {
        int alter = 2021 - geburtsjahr;
        return alter;
    }

    @Override
    public String toString() {
        return "ID: " + this.benutzername + " / Vorname: " + this.vorname + " / Nachname: " + this.nachname + " / Alter: " + this.alter;
    }
}