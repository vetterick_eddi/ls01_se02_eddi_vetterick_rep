﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       try (Scanner tastatur = new Scanner(System.in)) {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       zuZahlenderBetrag= 2.10;
       eingezahlterGesamtbetrag= 4.00;
       eingeworfeneMünze= 2.00;
       
       System.out.print("Zu zahlender Betrag: " + zuZahlenderBetrag + "0 Euro");
       
          //zuZahlenderBetrag = tastatur.nextDouble();

       // Geldeinwurf
       // -----------
       
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
         System.out.println("\nNoch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + "0 Euro");
         System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): 2.00 Euro");

           //eingeworfeneMünze = tastatur.nextDouble();
         
         eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       
       System.out.println("\n\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
        Thread.sleep(250);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
         System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + "0 Euro");
         System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
            System.out.println("2 Euro");
              rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
            System.out.println("1 Euro");
              rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
            System.out.println("50 Cent");
              rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
            System.out.println("20 Cent");
              rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
            System.out.println("10 Cent");
              rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
            System.out.println("5 Cent");
              rückgabebetrag -= 0.05;
           }
       }
  }
      
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}