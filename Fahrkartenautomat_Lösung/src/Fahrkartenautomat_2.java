import java.util.Scanner;

class Fahrkartenautomat_2 {
    public static void main(String[] args) {
       
    //    int anzahlTickets;
    //    double zuZahlenderBetrag;
    //    double ticketPreis;
    //    double eingezahlterGesamtbetrag;
    //    double eingeworfeneMuenze;
    //    double rueckgabebetrag;
    //    double eingegebenerBetrag;
    	fahrkartenBezahlen(fahrkartenbestellungErfassen());
    }
   public static double fahrkartenbestellungErfassen() {
	   double ticketPreis=0;
	   int anzahlTickets=0;
	   double zuZahlenderBetrag=0;
	   Scanner tastatur = new Scanner(System.in);
        System.out.print("Ticketpreis (Euro-Cent): ");
        ticketPreis = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        zuZahlenderBetrag = ticketPreis * anzahlTickets;{
	
        return zuZahlenderBetrag;
    }
   }
   public static void fahrkartenBezahlen(double zuZahlenderBetrag) {
	   double eingezahlterGesamtbetrag=0;
	   double eingeworfeneMuenze=0;
	   Scanner tastatur = new Scanner(System.in);
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
        	System.out.println("Noch zu zahlen: "+ String.format ("%1.2f",zuZahlenderBetrag - eingezahlterGesamtbetrag )+ " Euro");
            System.out.print("Eingabe (mind. 5 Cent, h�chstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        fahrkartenAusgeben();
        rueckgeldAusgeben(zuZahlenderBetrag,eingezahlterGesamtbetrag);
       
    }

   public static void warte(int millisekunde) {
	   try { 
		   Thread.sleep(millisekunde);
	       }
        catch (InterruptedException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
   }
    }
   public static void fahrkartenAusgeben() {

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 26; i++) {
            System.out.print("=");
            
        warte(100);
            }
        }       
    
   public static void muenzeAusgeben(int betrag,String einheit) {
	   
	   System.out.println(betrag + " " + einheit);
	   
   }
   public static void rueckgeldAusgeben(double zuZahlenderBetrag,double eingezahlterGesamtbetrag) {
	   double rueckgabebetrag=0;

        System.out.println("\n\n");

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.0) {
            //***** L�sung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der R�ckgabebetrag in H�he von ", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                muenzeAusgeben(2,"Euro");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1,"Euro");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50,"Cent");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20,"Cent");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10,"Cent");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5,"Cent");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}