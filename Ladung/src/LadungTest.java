
public class LadungTest {

	public static void main(String[] args) {
		
		Ladung l1 = new Ladung();
		
		l1.setBezeichnung("Ferengi Schneckensaft");
		l1.setMenge(200);
		
		System.out.println("Ladung l1");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		System.out.println("");
		
		
		Ladung l2 = new Ladung();
		l2.setBezeichnung("Bat'leth Klingonen Schwert");
		l2.setMenge(200);
		
		System.out.println("Ladung l2");
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		System.out.println("");
		
		
		Ladung l3 = new Ladung();
		l3.setBezeichnung("Borg-Schrott");
		l3.setMenge(5);
		
		System.out.println("Ladung l3");
		System.out.println(l3.getBezeichnung());
		System.out.println(l3.getMenge());
		System.out.println("");
		
		Ladung l4 = new Ladung();
		l4.setBezeichnung("Rote Materie");
		l4.setMenge(2);
		
		System.out.println("Ladung l4");
		System.out.println(l4.getBezeichnung());
		System.out.println(l4.getMenge());
		System.out.println("");
		
		Ladung l5 = new Ladung();
		l5.setBezeichnung("Plasma-Waffe");
		l5.setMenge(50);
		
		System.out.println("Ladung l5");
		System.out.println(l5.getBezeichnung());
		System.out.println(l5.getMenge());
		System.out.println("");
		
		
		Ladung l6 = new Ladung();
		l6.setBezeichnung("Forschungssonde");
		l6.setMenge(35);
		
		System.out.println("Ladung l6");
		System.out.println(l6.getBezeichnung());
		System.out.println(l6.getMenge());
		System.out.println("");
		
		Ladung l7 = new Ladung();
		l7.setBezeichnung("Photonentorpedo");
		l7.setMenge(3);
		
		System.out.println("Ladung l7");
		System.out.println(l7.getBezeichnung());
		System.out.println(l7.getMenge());
		System.out.println("");
		
		
		
		
		
		
		
		

	}

}