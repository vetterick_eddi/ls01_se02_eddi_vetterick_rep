
public class Raumschiff {

	private int PhotonentorpedoAnzahl;
	private int EnergieversorgungInProzent;
	private int SchildeInProzent;
	private int HuelleInProzent;
	private int LebenserhaltungssystemeInProzent;
	private int AndroidenAnzahl;
	private String Schiffsname;
	
	public Raumschiff(int PhotonentorpedoAnzahl, int EnergieversorgungInProzent, int SchildeInProzent, int HuelleInProzent,
					  int LebenserhaltungssystemeInProzent, int AndroidenAnzahl, String Schiffsname ) {
		
		this.PhotonentorpedoAnzahl = PhotonentorpedoAnzahl;
		this.EnergieversorgungInProzent = EnergieversorgungInProzent;
		this.SchildeInProzent = SchildeInProzent;
		this.HuelleInProzent = HuelleInProzent;
		this.LebenserhaltungssystemeInProzent = LebenserhaltungssystemeInProzent;
		this.AndroidenAnzahl = AndroidenAnzahl;
		this.Schiffsname = Schiffsname;
	}
	
	public Raumschiff() {
		
		this.PhotonentorpedoAnzahl = 0;
		this.EnergieversorgungInProzent = 0;
		this.SchildeInProzent = 0;
		this.HuelleInProzent = 0;
		this.LebenserhaltungssystemeInProzent = 0;
		this.AndroidenAnzahl = 0;
		this.Schiffsname = "Enterprise";

	}
	

	public void setPhotonentorpedoAnzahl (int PhotonentorpedoAnzahl) {
		this.PhotonentorpedoAnzahl = PhotonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return PhotonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int EnergieversorgungInProzent) {
		this.EnergieversorgungInProzent = EnergieversorgungInProzent;
		
	}
	public int getEnergieversorgungInProzent() {
		return EnergieversorgungInProzent;
	}
	
	public void setSchildeInProzent (int SchildeInProzent) {
		this.SchildeInProzent = SchildeInProzent;
	}

	public int getSchildeInProzent() {
		return SchildeInProzent;
	}

	public void setHuelleInProzent(int HuelleInProzent) {
		this.HuelleInProzent = HuelleInProzent;
	
	}
	public int getHuelleInProzent() {
		return HuelleInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent (int LebenserhaltungssystemeInProzent) {
		this.LebenserhaltungssystemeInProzent = LebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return LebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int AndroidenAnzahl) {
		this.AndroidenAnzahl = AndroidenAnzahl;
			
	}
	public int getAndroidenAnzahl() {
		return AndroidenAnzahl;
	}
	
	public void setSchiffsname (String Schiffsname) {
		this.Schiffsname = Schiffsname;
	}

	public String getSchiffsname() {
		return Schiffsname;
	}

}