
public class RaumschiffTest {

	public static void main(String[] args) {
		
		Raumschiff r1 = new Raumschiff();
		
		r1.setSchiffsname("IKS Hegh'ta");
        r1.setPhotonentorpedoAnzahl(1);
        r1.setEnergieversorgungInProzent(100);
        r1.setSchildeInProzent(100);
        r1.setHuelleInProzent(100);
        r1.setLebenserhaltungssystemeInProzent(100);
        r1.setAndroidenAnzahl(2);
		
        System.out.println(r1.getSchiffsname());
        System.out.println("");
        System.out.println(r1.getPhotonentorpedoAnzahl());
		System.out.println(r1.getEnergieversorgungInProzent());
		System.out.println(r1.getSchildeInProzent());
		System.out.println(r1.getHuelleInProzent());
		System.out.println(r1.getLebenserhaltungssystemeInProzent());
		System.out.println(r1.getAndroidenAnzahl());
		System.out.println("");
		System.out.println("");
		
		Raumschiff r2 = new Raumschiff();

		r2.setSchiffsname("IRW Khazara");
        r2.setPhotonentorpedoAnzahl(2);
        r2.setEnergieversorgungInProzent(100);
        r2.setSchildeInProzent(100);
        r2.setHuelleInProzent(100);
        r2.setLebenserhaltungssystemeInProzent(100);
        r2.setAndroidenAnzahl(2);
		
        System.out.println(r2.getSchiffsname());
        System.out.println("");
        System.out.println(r2.getPhotonentorpedoAnzahl());
		System.out.println(r2.getEnergieversorgungInProzent());
		System.out.println(r2.getSchildeInProzent());
		System.out.println(r2.getHuelleInProzent());
		System.out.println(r2.getLebenserhaltungssystemeInProzent());
		System.out.println(r2.getAndroidenAnzahl());
		System.out.println("");
		System.out.println("");
	
		Raumschiff r3 = new Raumschiff();

		r3.setSchiffsname("Ni'Var");
        r3.setPhotonentorpedoAnzahl(0);
        r3.setEnergieversorgungInProzent(80);
        r3.setSchildeInProzent(80);
        r3.setHuelleInProzent(50);
        r3.setLebenserhaltungssystemeInProzent(100);
        r3.setAndroidenAnzahl(5);
		
        System.out.println(r3.getSchiffsname());
        System.out.println("");
        System.out.println(r3.getPhotonentorpedoAnzahl());
		System.out.println(r3.getEnergieversorgungInProzent());
		System.out.println(r3.getSchildeInProzent());
		System.out.println(r3.getHuelleInProzent());
		System.out.println(r3.getLebenserhaltungssystemeInProzent());
		System.out.println(r3.getAndroidenAnzahl());
		
		
		
	}

}